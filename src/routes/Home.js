import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import WelcomeScreen from './Welcome'
import ContactScreen from './Contact'
import React from 'react';
import { useTranslation } from 'react-i18next';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Tab = createBottomTabNavigator();

function Home() {
    const { t, i18n } = useTranslation();
  return (
    <Tab.Navigator>
      <Tab.Screen 
        name="Welcome"
        options={{
          tabBarLabel: t('Welcome'),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name='home-outline' color={color} size={size} />
          ),
          headerShown: false,
        }}
        component={WelcomeScreen} 
      />
      <Tab.Screen 
        name="Contact"
        options={{
          tabBarLabel: t('Contact'),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name='account-box-outline' color={color} size={size} />
          ),
          headerShown: false,
        }}
        component={ContactScreen} 
      />
    </Tab.Navigator>
  );
}

export default Home;
import { useTranslation } from 'react-i18next';
import React from 'react';
import { Text, View } from 'react-native';

function Contact() {
    const { t, i18n } = useTranslation();
    return (
      <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
        <Text style={{fontSize: 20, padding: 12}}>{t('Contact message')}</Text>
        <Text style={{fontSize: 16, padding: 24}}>{t('Email')+'kkericgp@gmail.com'}</Text>
        <Text style={{fontSize: 16, padding: 24}}>{t('phoneNo')+'91541480'}</Text>
      </View>
    );
  }
  
export default Contact;
import React from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';
import { useTranslation } from 'react-i18next';

function Login (props) {
  const {navigation} = props;
  const { t, i18n } = useTranslation();
  const [text_ID, onChangeID] = React.useState(null);
  const [text_PW, onChangePW] = React.useState(null);
  return (
      <View>
        <Text style={styles.login_text_prompt}>
          {t('Login Prompt')}
        </Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeID}
          placeholder='ID'
          value={text_ID}
        />
        <TextInput
          secureTextEntry={true} 
          style={styles.input}
          onChangeText={onChangePW}
          placeholder={t('pw')}
          value={text_PW}
          textContentType='password'
        />
        <View style={styles.btn_login}>
          <Button
            onPress={() => {
              (text_ID == 'admin' && text_PW == 'Admin&8181')
                ? navigation.replace('Home')
                : alert(t('Wrong IDPW'));
            }}
            title={t('Login')}
            color='#007aff'
          />
        </View>
        <View style={styles.btn_language}>
          <Button
              onPress={() => {
                i18n.changeLanguage('cn')
              }}          
              title='CN'
              color={i18n.language=='cn'?'#007aff':'gray'}
          />
            <Button
            onPress={() => {
              i18n.changeLanguage('en')
            }}
            title='EN'
            color={i18n.language=='en'?'#007aff':'gray'}
          />
          <View>
          </View>
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  login_text_prompt: {
    padding: 10,
    fontSize: 16,
    height: 40,
  },  
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  btn_login: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  btn_language: {
    paddingTop: 40,
    padding: 12,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
});

export default Login;
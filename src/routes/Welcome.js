import { useTranslation } from 'react-i18next';
import React from 'react';
import { Text, View } from 'react-native';

function Welcome() {
    const { t, i18n } = useTranslation();
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>{t('Welcome message')}</Text>
      </View>
    );
  }
  
export default Welcome;